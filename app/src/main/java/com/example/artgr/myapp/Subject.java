package com.example.artgr.myapp;

/**
 * Created by artgr on 5/18/2018.
 */

public class Subject {
    public String name;
    public boolean value;

    public Subject(String n, boolean b){
        name = n;
        value = b;
    }
}
