package com.example.artgr.myapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RequestActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    LinearLayout dismissed, sended, accepted, received;
    String myKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dismissed = (LinearLayout) findViewById(R.id.dismissed);
        sended = (LinearLayout) findViewById(R.id.sended);
        accepted = (LinearLayout) findViewById(R.id.accepted);
        received = (LinearLayout) findViewById(R.id.received);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Integer key1 = user.getEmail().hashCode();
        myKey = key1.toString();
        DatabaseReference me = mDatabase.child("users").child(myKey).child("send");
        me.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.hasChildren()){
                    for (DataSnapshot child : snapshot.getChildren()) {
                        setSend(child.getKey().toString());
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        DatabaseReference me2 = mDatabase.child("users").child(myKey).child("received");
        me2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.hasChildren()){
                    for (DataSnapshot child : snapshot.getChildren()) {
                        setResive(child.getKey().toString());
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    void setResive(String key){
        DatabaseReference me = mDatabase.child("users").child(myKey).child("received").child(key);
        me.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                if(snapshot != null  && snapshot.getValue() != null){
                    LinearLayout l = new LinearLayout(RequestActivity.this);
                    l.setOrientation(LinearLayout.VERTICAL);

                    LinearLayout l1 = new LinearLayout(RequestActivity.this);
                    l1.setOrientation(LinearLayout.HORIZONTAL);

                    Button b= new Button(RequestActivity.this);
                    b.setContentDescription(snapshot.getKey());
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(RequestActivity.this, UserViewActivity.class);
                            intent.putExtra("name", snapshot.getKey());
                            startActivity(intent);
                        }
                    });
                    func(b);


                    Button b1 = new Button(RequestActivity.this);
                    b1.setText("accept");
                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            Integer key1 = user.getEmail().hashCode();
                            DatabaseReference me = mDatabase.child("users").child(key1.toString()).child("received").child(snapshot.getKey());
                            DatabaseReference you = mDatabase.child("users").child(snapshot.getKey()).child("send").child(key1.toString());
                            me.setValue("true");
                            you.setValue("true");
                            Intent intent = new Intent(RequestActivity.this, RequestActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    Button b2 = new Button(RequestActivity.this);
                    b2.setText("dismiss");
                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            Integer key1 = user.getEmail().hashCode();
                            DatabaseReference me = mDatabase.child("users").child(key1.toString()).child("received").child(snapshot.getKey());
                            DatabaseReference you = mDatabase.child("users").child(snapshot.getKey()).child("send").child(key1.toString());
                            me.setValue("dismiss");
                            you.setValue("dismiss");
                            Intent intent = new Intent(RequestActivity.this, RequestActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    b1.setWidth(485);
                    b2.setWidth(485);
                    b1.setBackgroundColor(Color.parseColor("#ffffff"));
                    b2.setBackgroundColor(Color.parseColor("#ffffff"));

                    switch (snapshot.getValue().toString()){
                        case "true"  :
                            accepted.addView(b, accepted.getChildCount());
                            break;
                        case "false" :
                            l1.addView(b1, 0);
                            l1.addView(b2, 1);
                            l.addView(b, 0);
                            l.addView(l1, 1);
                            received.addView(l, received.getChildCount());
                            break;
                        case "dismiss" :
                            dismissed.addView(b, dismissed.getChildCount());
                            break;
                    }

                }else{}
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    void setSend(String key){
        DatabaseReference me = mDatabase.child("users").child(myKey).child("send").child(key);
        me.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                if(snapshot != null && snapshot.getValue() != null){
                    Button b = new Button(RequestActivity.this);
                    b.setContentDescription(snapshot.getKey());
                    func(b);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(RequestActivity.this, UserViewActivity.class);
                            intent.putExtra("name", snapshot.getKey());
                            startActivity(intent);
                        }
                    });

                    switch (snapshot.getValue().toString()){
                        case "true"  :
                            accepted.addView(b, accepted.getChildCount());
                            break;
                        case "false" :
                            sended.addView(b, sended.getChildCount());
                            break;
                        case "dismiss" :
                            dismissed.addView(b, dismissed.getChildCount());
                            break;
                    }

                }else{}
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    void func(final Button button){
        DatabaseReference f = mDatabase.child("users").child(button.getContentDescription().toString());
        if( f != null) {
            f.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    snapshot.child("name").getValue().toString();
                    StringBuilder str = new StringBuilder();
                    str.append(snapshot.child("name").getValue().toString());
                    str.append(" ");
                    str.append(snapshot.child("surname").getValue().toString());
                    button.setText(str.toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }

            });
        }


    }
}
