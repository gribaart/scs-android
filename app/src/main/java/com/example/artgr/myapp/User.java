package com.example.artgr.myapp;

import java.util.ArrayList;

/**
 * Created by artgr on 5/16/2018.
 */

public class User {

    public String email;
    public String name;
    public String surname;
    public String university;
    public String faculty;
    public String speciality;
    public String city;
    public String number;
    public String avatar;
    ArrayList<String> desire = new ArrayList<String>();
    ArrayList<String> offer = new ArrayList<String>();

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String email, String name, String surname, String university, String faculty,
                String speciality, String city, String number, String avatar) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.university = university;
        this.faculty = faculty;
        this.speciality = speciality;
        this.city = city;
        this.number = number;
        this.avatar = avatar;
    }

}