package com.example.artgr.myapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.util.Date;

public class SetInfoActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private FirebaseAuth mAuth;
    ImageView mImageView;
    Button button;
    EditText name;
    EditText surname;
    EditText university;
    EditText faculty;
    EditText speciality;
    EditText city;
    EditText number;
    String avatar = "";
    private DatabaseReference mDatabase;
    private StorageReference mStorageRef;

    private View mProgressView;
    private View mLoginFormView;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_info);
        mLoginFormView = findViewById(R.id.sc_progress);
        mProgressView = findViewById(R.id.dw_progress);
        showProgress(true);
        mImageView = (ImageView) findViewById(R.id.imageView2);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });



         name = (EditText) findViewById(R.id.name);
         surname = (EditText) findViewById(R.id.surname);
         university = (EditText) findViewById(R.id.university);
         faculty = (EditText) findViewById(R.id.faculty);
         speciality = (EditText) findViewById(R.id.speciality);
         city = (EditText) findViewById(R.id.city);
         number = (EditText) findViewById(R.id.number);



        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        button = (Button) findViewById(R.id.save_info_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth = FirebaseAuth.getInstance();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String email = user.getEmail().toString();
                writeNewUser(email,  name.getText().toString(),  surname.getText().toString(),
                            university.getText().toString(), faculty.getText().toString(),
                            speciality.getText().toString(),  city.getText().toString(),
                            number.getText().toString(), avatar);
                Intent intent = new Intent(SetInfoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        try {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            Integer key = user.getEmail().hashCode();
            DatabaseReference f = mDatabase.child("users").child(key.toString());
            if( f != null) {
                f.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if( snapshot.child("name").getValue() != null) {
                        name.setText(snapshot.child("name").getValue().toString());
                        surname.setText(snapshot.child("surname").getValue().toString());
                        university.setText(snapshot.child("university").getValue().toString());
                        faculty.setText(snapshot.child("faculty").getValue().toString());
                        speciality.setText(snapshot.child("speciality").getValue().toString());
                        city.setText(snapshot.child("city").getValue().toString());
                        number.setText(snapshot.child("number").getValue().toString());
                        avatar = snapshot.child("avatar").getValue().toString();
                        if (!avatar.equals("")) {
                            DownloadImageTask dw = new DownloadImageTask((ImageView) findViewById(R.id.imageView2));
                            dw.execute(avatar);
                        }}
                        showProgress(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }

                });
            }
        }catch (Exception e){}
    }

    private void writeNewUser(String email, String name, String surname, String university, String faculty, String speciality, String city, String number, String avatar) {
        User user = new User( email,  name,  surname,  university,  faculty,  speciality,  city,  number, avatar);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Integer key = email.hashCode();
        mDatabase.child("users").child(key.toString()).setValue(user);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            byte[] byteAvater = getImageData(imageBitmap);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String email = user.getEmail().toString();
            Integer key = email.hashCode();
            StorageReference mountainsRef = mStorageRef.child(key + ".jpg");
            UploadTask uploadTask = mountainsRef.putBytes(byteAvater);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    avatar = taskSnapshot.getDownloadUrl().toString();
                    new DownloadImageTask(mImageView)
                            .execute(taskSnapshot.getDownloadUrl().toString());
                }
            });

        }
    }



    public byte[] getImageData(Bitmap bmp) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        return data;
    }
}


