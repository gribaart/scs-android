package com.example.artgr.myapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;

public class MyProfileActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private FirebaseAuth mAuth;
    ImageView mImageView;
    Button btn_edit_info, btn_edit_desire, btn_edit_offer;
    TextView name,surname,university, faculty, speciality, city, number;
    String avatar;
    LinearLayout linearDesire, linearOffer;
    private DatabaseReference mDatabase;
    private StorageReference mStorageRef;
    private View mProgressView;
    private View mLoginFormView;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        mLoginFormView = findViewById(R.id.sc4_progress);
        mProgressView = findViewById(R.id.dw4_progress);
        showProgress(true);
        linearDesire = (LinearLayout) findViewById(R.id.linear_desire);
        linearOffer = (LinearLayout) findViewById(R.id.linear_offer);
        mImageView = (ImageView) findViewById(R.id.ava);
        name = (TextView) findViewById(R.id.name);
        surname = (TextView) findViewById(R.id.surname);
        university = (TextView) findViewById(R.id.university);
        faculty = (TextView) findViewById(R.id.faculty);
        speciality = (TextView) findViewById(R.id.speciality);
        city = (TextView) findViewById(R.id.city);
        number = (TextView) findViewById(R.id.number);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        btn_edit_info  = (Button) findViewById(R.id.btn_edit_info);
        btn_edit_desire  = (Button) findViewById(R.id.btn_edit_desire);
        btn_edit_offer  = (Button) findViewById(R.id.btn_edit_offer);
        btn_edit_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, SetInfoActivity.class);
                startActivity(intent);
            }
        });
        btn_edit_desire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, DesireActivity.class);
                startActivity(intent);
            }
        });
        btn_edit_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, OfferActivity.class);
                startActivity(intent);
            }
        });

        try {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            Integer key = user.getEmail().hashCode();
            DatabaseReference f = mDatabase.child("users").child(key.toString());
            if( f != null) {
                f.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if( snapshot.child("name").getValue() != null) {
                            name.setText(snapshot.child("name").getValue().toString());
                            surname.setText(snapshot.child("surname").getValue().toString());
                            university.setText(snapshot.child("university").getValue().toString());
                            faculty.setText(snapshot.child("faculty").getValue().toString());
                            speciality.setText(snapshot.child("speciality").getValue().toString());
                            city.setText(snapshot.child("city").getValue().toString());
                            number.setText(snapshot.child("number").getValue().toString());
                            avatar = snapshot.child("avatar").getValue().toString();
                            if (!avatar.equals("")) {
                                new DownloadImageTask((ImageView) findViewById(R.id.ava)).execute(avatar);
                            }


                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }

                });

                DatabaseReference des = mDatabase.child("users").child(key.toString()).child("desire");
                des.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        for(DataSnapshot child : snapshot.getChildren() ){
                            TextView text = new TextView(MyProfileActivity.this);
                            text.setText(child.getValue().toString());
                            int index = linearDesire.getChildCount();
                            linearDesire.addView(text, index);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                DatabaseReference off = mDatabase.child("users").child(key.toString()).child("offer");
                off.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        for(DataSnapshot child : snapshot.getChildren() ){
                            TextView text = new TextView(MyProfileActivity.this);
                            text.setText(child.getValue().toString());
                            int index = linearOffer.getChildCount();
                            linearOffer.addView(text, index);
                        }
                        showProgress(false);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }

                });


            }
        }catch (Exception e){}

    }
}
