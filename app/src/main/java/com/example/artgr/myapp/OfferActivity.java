package com.example.artgr.myapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.ArrayList;


public class OfferActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    LinearLayout linearMain;
    CheckBox checkBox;
    Button button;
    ArrayList<String> offer = new ArrayList<String>();
    private View mProgressView;
    private View mLoginFormView;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    void addCheckBoxes(){

        linearMain = (LinearLayout) findViewById(R.id.linear_main);
        ArrayList<String> al = new ArrayList<String>();
        al.add("Linear algebra");
        al.add("Discrete Math");
        al.add("Mathematical analysis");
        al.add("Math statistics");
        al.add("Geometry");
        al.add("Optimization");
        al.add("Algorithmization");
        al.add("Game theory");
        al.add("English");
        al.add("Russian");
        al.add("Czech");
        al.add("Economy");
        al.add("Economic statistics");
        al.add("Project management");
        al.add("Software architecture");
        al.add("Web applications");
        al.add("Mobile applications");
        al.add("Programming in Java");
        al.add("Programming in JS");
        al.add("Programming in PHP");
        al.add("Programming in HTML");
        al.add("Programming in CSS");
        al.add("Programming in Phyton");

        for (int i = 0; i < al.size(); i++){

            checkBox = new CheckBox(OfferActivity.this);
            checkBox.setId(i);
            checkBox.setText(al.get(i));
            checkBox.setOnClickListener(getClickOnSomething(checkBox));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    final String email = user.getEmail().toString();
                    Integer key = email.hashCode();
                    if(isChecked){
                        mDatabase.child("subjects").child(buttonView.getText().toString()).child("offer").child(key.toString()).setValue(email);
                    }else {
                        mDatabase.child("subjects").child(buttonView.getText().toString()).child("offer").child(key.toString()).getRef().removeValue();
                    }
                }
            });
            linearMain.addView(checkBox, i);
        }
    }

    void setOfferedCheckboxes(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String email = user.getEmail().toString();
        Integer key = email.hashCode();
        DatabaseReference f = mDatabase.child("users").child(key.toString()).child("offer");
        f.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String json = null;
                String[] array = {};
                try {
                    json = snapshot.getValue().toString();
                    array = json.split(", ");
                    array[0] = array[0].substring(1);
                    array[array.length - 1] = array[array.length - 1].substring(0,array[array.length - 1].length() - 1);
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
                }


                for (int i = 0; i < linearMain.getChildCount(); i++) {
                    for (String s : array) {
                        CheckBox cb = (CheckBox) linearMain.getChildAt(i);
                        if(s.equals(cb.getText())){
                            cb.setChecked(true);
                        }
                    }
                }

                foo();


            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    void hideDesireBoxes(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String email = user.getEmail().toString();
        Integer key = email.hashCode();
        DatabaseReference f = mDatabase.child("users").child(key.toString()).child("desire");
        f.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String json = null;
                String[] array = {};
                try {
                    json = snapshot.getValue().toString();
                    array = json.split(", ");
                    array[0] = array[0].substring(1);
                    array[array.length - 1] = array[array.length - 1].substring(0,array[array.length - 1].length() - 1);
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
                }


                for (int i = 0; i < linearMain.getChildCount(); i++) {
                    for (String s : array) {
                        CheckBox cb = (CheckBox) linearMain.getChildAt(i);
                        if(cb != null) {
                            if (s.equals(cb.getText())) {
                                linearMain.removeViewAt(i);
                            }
                        }
                    }
                }


            foo();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        mLoginFormView = findViewById(R.id.sc3_progress);
        mProgressView = findViewById(R.id.dw3_progress);
        showProgress(true);


        button = (Button) findViewById(R.id.save_offer);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < linearMain.getChildCount(); i++) {
                    CheckBox cb = (CheckBox) linearMain.getChildAt(i);
                    if(cb.isChecked()){
                        offer.add(cb.getText().toString());
                    }
                }
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String email = user.getEmail().toString();
                Integer key = email.hashCode();
                mDatabase.child("users").child(key.toString()).child("offer").setValue(offer);
                Intent intent = new Intent(OfferActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        addCheckBoxes();
        setOfferedCheckboxes();
        hideDesireBoxes();

    }

    View.OnClickListener getClickOnSomething(final Button button){
        return new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                Log.d("Offer to ", button.getText().toString());
            }
        };
    }

    int cout = 0;
    void foo(){
        cout++;
        if(cout == 2){
            cout = 0;
            showProgress(false);
        }

    }
}
