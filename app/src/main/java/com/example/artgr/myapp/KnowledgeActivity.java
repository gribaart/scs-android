package com.example.artgr.myapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class KnowledgeActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private Map<String,ArrayList<String>> list = new HashMap<String,ArrayList<String>>();
    ConstraintLayout cl;
    LinearLayout linearMain;
    String[] array = {};
    int ctr = 0;
    private View mProgressView;
    private View mLoginFormView;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge);
        mLoginFormView = findViewById(R.id.sc7_progress);
        mProgressView = findViewById(R.id.dw7_progress);
        showProgress(true);
        linearMain = (LinearLayout) findViewById(R.id.linear_main);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String email = user.getEmail().toString();
        Integer key = email.hashCode();
        DatabaseReference f = mDatabase.child("users").child(key.toString()).child("desire");
        f.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String json = null;
                try {
                    json = snapshot.getValue().toString();
                    array = json.split(", ");
                    array[0] = array[0].substring(1);
                    array[array.length - 1] = array[array.length - 1].substring(0,array[array.length - 1].length() - 1);
                    Log.e("My App - ", json);

                    DatabaseReference ref;
                    linearMain = (LinearLayout) findViewById(R.id.linear_kn);
                    StringBuilder str = new StringBuilder();
                    for (int i = 0; i < array.length; i++) {
                        ref = mDatabase.child("subjects").child(array[i]).child("offer");
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if(snapshot.hasChildren()) {
                                    LinearLayout l = new LinearLayout(KnowledgeActivity.this);
                                    l.setPadding(0,0,0,1);
                                    l.setOrientation(LinearLayout.VERTICAL);
                                    TextView text = new TextView(KnowledgeActivity.this);
                                    text.setText(array[ctr]);

                                    float Textsize = text.getTextSize();
                                    text.setTextSize(Textsize + 1);
                                    ctr++;
                                    int index = l.getChildCount();
                                    l.addView(text, index);
                                    for (DataSnapshot child : snapshot.getChildren()) {
                                        Button bb = new Button(KnowledgeActivity.this);
                                        bb.setContentDescription(child.getValue().toString());
                                        func(bb);
                                        bb.setOnClickListener(getClickOnSomething(bb));
                                        index = l.getChildCount();
                                        l.addView(bb, index);
                                    }
                                    index = linearMain.getChildCount();
                                    linearMain.addView(l, index);
                                }
                                foo();
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                ctr = 0;
                            }
                        });
                    }

                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });



    }

    View.OnClickListener getClickOnSomething(final Button text){
        return new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                Intent intent = new Intent(KnowledgeActivity.this, ProfileActivity.class);
                intent.putExtra("name", text.getContentDescription().toString());
                startActivity(intent);
            }
        };
    }

    void func(final Button button){

        button.setText(button.getContentDescription().toString());
        Integer key = button.getText().toString().hashCode();
        DatabaseReference f = mDatabase.child("users").child(key.toString());
        if( f != null) {
            f.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    StringBuilder str = new StringBuilder();
                    str.append(snapshot.child("name").getValue().toString());
                    str.append(" ");
                    str.append(snapshot.child("surname").getValue().toString());
                    button.setText(str.toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }

            });
        }


    }


    void foo(){
            showProgress(false);
    }
}
